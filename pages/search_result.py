from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import pages.search_result
from pages import project_page
from pages.project_page import AddProjectPage, project_names


class SearchResultsPage:
    URL = 'http://demo.testarena.pl/administration/projects'

    def __init__(self, browser):
    


        self.browser = browser


    def load(self):
        self.browser.get(self.URL)




    def search_last(self):
        project_page = AddProjectPage(self.browser)
        search_input = self.browser.find_element(By.CSS_SELECTOR, "#search")

        search_input.send_keys(project_names[-1])


        self.browser.find_element(By.CSS_SELECTOR, "#j_searchButton").click()








    def find_last_created_project(self):


        project_element = WebDriverWait(self.browser, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "tbody tr:nth-child(1) td:nth-child(1)"))
        )

        return project_element.text == project_names[-1]






